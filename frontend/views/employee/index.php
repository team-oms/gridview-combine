<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= $this->title ?>- <small>Please mail me your feedback <b><a href="mailto:coder@omsc.in">coder@omsc.in</a></b></small></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <? #Html::a('Create Employee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?
 
    
    $gridColumns = [
                    [
                        'class'=>'kartik\grid\SerialColumn',
                    ],
                    /*[
                        'class'=>'kartik\grid\RadioColumn',
                        'width'=>'36px',
                        'headerOptions'=>['class'=>'kartik-sheet-style'],
                    ],*/
                    [
                        'class'=>'kartik\grid\ExpandRowColumn',
                        'width'=>'50px',                        
                      //  'group'=>true,
                        'value'=>function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detail'=>function ($model, $key, $index, $column) {
                            return Yii::$app->controller->renderPartial('_expand-row-details', ['model'=>$model,'id'=>$key]);
                        },
                        'headerOptions'=>['class'=>'kartik-sheet-style'], 
                        'expandOneOnly'=>true
                    ],
                    
                    [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'name',
                        'pageSummary'=>'Total',
                                             
                        'group'=>true,
                       // 'subGroupOf'=>1,
                        'groupHeader'=>function ($model, $key, $index, $widget) { // Closure method
                                return [
                                    'mergeColumns'=>[[2,4]], // columns to merge in summary
                                    'content'=>[             // content to show in each summary cell
                                        2=>'Summary (' . $model->name  . ')',
                                        //4=>GridView::F_AVG,
                                        //5=>GridView::F_SUM,
                                       // 6=>GridView::F_SUM,
                                    ],
                                    'contentFormats'=>[      // content reformatting for each summary cell
                                        4=>['format'=>'number', 'decimals'=>2],
                                        //5=>['format'=>'number', 'decimals'=>0],
                                        //6=>['format'=>'number', 'decimals'=>2],
                                    ],
                                    'contentOptions'=>[      // content html attributes for each summary cell
                                        2=>['style'=>'font-variant:small-caps'],
                                        4=>['style'=>'text-align:right'],
                                        //5=>['style'=>'text-align:right'],
                                        //6=>['style'=>'text-align:right'],
                                    ],
                                    // html attributes for group summary row
                                    'options'=>['class'=>'danger','style'=>'font-weight:bold;']
                                ];
                            },
                        'editableOptions'=>function ($model, $key, $index) {
                        return [
                            'formOptions'=>['action'=>Url::to(['employee/change','id'=>$model->id])],
                            'header'=>'Buy Amount', 
                           //'inputType'=>\kartik\editable\Editable::INPUT_SPIN,
                            
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,]
                            ]
                        ];},
                        'vAlign'=>'top',
                        'width'=>'210px',
                        'readonly'=>function($model, $key, $index, $widget) {
                            return (!$model->status); // do not allow editing of inactive records
                        },
                       
                    ],
                     [
            'attribute'=>'place', 
            'width'=>'250px',
            'value'=>function ($model, $key, $index, $widget) { 
                return $model->place0->name;
            },
            'filterType'=>GridView::FILTER_SELECT2,
            'filter'=>\yii\helpers\ArrayHelper::map(\app\models\Country::find()->asArray()->all(), 'id', 'name'), 
            'filterWidgetOptions'=>[
                'pluginOptions'=>['allowClear'=>true],
            ],
            'filterInputOptions'=>['placeholder'=>'Any Country'],
            'group'=>true,  // enable grouping
            'subGroupOf'=>2, // supplier column index is the parent group,
            'groupFooter'=>function ($model, $key, $index, $widget) { // Closure method
                return [
                    'mergeColumns'=>[[3, 4]], // columns to merge in summary
                    'content'=>[              // content to show in each summary cell
                        3=>'Summary (' . $model->place0->name . ')',
                        //4=>GridView::F_AVG,
                        //5=>GridView::F_SUM,
                        //6=>GridView::F_SUM,
                    ],
                    'contentFormats'=>[      // content reformatting for each summary cell
                        4=>['format'=>'number', 'decimals'=>2],
                        5=>['format'=>'number', 'decimals'=>0],
                        6=>['format'=>'number', 'decimals'=>2],
                    ],
                    'contentOptions'=>[      // content html attributes for each summary cell
                        4=>['style'=>'text-align:right'],
                        5=>['style'=>'text-align:right'],
                        6=>['style'=>'text-align:right'],
                    ],
                    // html attributes for group summary row
                    'options'=>['class'=>'success','style'=>'font-weight:bold;']
                ];
            },
        ],
                 
                    [
                        'class'=>'kartik\grid\EditableColumn',
                        'attribute'=>'age', 
                        'readonly'=>function($model, $key, $index, $widget) {
                            return (!$model->status); // do not allow editing of inactive records
                        },
                        'editableOptions'=>function ($model, $key, $index) {
                        return [
                            'formOptions'=>['action'=>Url::to(['employee/change','id'=>$model->id])],
                            'header'=>'Buy Amount', 
                            'inputType'=>\kartik\editable\Editable::INPUT_SPIN,
                            
                            'options'=>[
                                'pluginOptions'=>['min'=>0, 'max'=>5000,]
                            ]
                        ];},
                        'hAlign'=>'right', 
                        'vAlign'=>'middle',
                        'width'=>'7%',
                        'format'=>['decimal', 2],
                        //'pageSummary'=>true
                    ],
                      [
                        'attribute'=>'place',
                        'filter'=>false,
                        'value'=>function ($model, $key, $index, $widget) {
                            return $model->place ;
                        },
                        'width'=>'8%',
                        'vAlign'=>'middle',
                        'format'=>'raw',
                        'noWrap'=>1
                    ],
                    /*
                    [
                        'attribute'=>'status', 
                        'vAlign'=>'middle',
                        'hAlign'=>'right', 
                        'width'=>'7%',
                        'format'=>['decimal', 2],
                        'pageSummary'=>true
                    ],*/
                    
                    
                    ];
                            
                            ?>
    <?= GridView::widget([
    'dataProvider'=> $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'responsive'=>true,
    'hover'=>true
]); ?>

</div>
