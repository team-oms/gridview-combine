<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "works".
 *
 * @property integer $id
 * @property integer $empId
 * @property string $workName
 * @property integer $payment
 * @property integer $status
 *
 * @property Employee $emp
 */
class Works extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'works';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['empId', 'workName', 'payment'], 'required'],
            [['empId', 'payment', 'status'], 'integer'],
            [['workName'], 'string', 'max' => 200],
            [['empId'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['empId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empId' => 'Emp ID',
            'workName' => 'Work Name',
            'payment' => 'Payment',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmp()
    {
        return $this->hasOne(Employee::className(), ['id' => 'empId']);
    }
}
