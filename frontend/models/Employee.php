<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property string $name
 * @property integer $age
 * @property integer $place
 * @property integer $status
 *
 * @property Country $place0
 * @property Works[] $works
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'age', 'place'], 'required'],
            [['age', 'place', 'status'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['place'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['place' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'age' => 'Age',
            'place' => 'Place',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace0()
    {
        return $this->hasOne(Country::className(), ['id' => 'place']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorks()
    {
        return $this->hasMany(Works::className(), ['empId' => 'id']);
    }
}
